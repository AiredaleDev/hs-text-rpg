# Revision history for nostalgia-game

## 0.1.0.0 -- 2022-05-22

* First version. Released on an unsuspecting world.
* They definitely weren't ready for what was coming.
