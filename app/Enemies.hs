module Enemies where

data Enemy = Enemy
  { eHP    :: Int
  , eMaxHP :: Int
  } deriving Eq

enemies :: [(String, Enemy)]
enemies = []
