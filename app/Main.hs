{-# LANGUAGE DeriveGeneric #-}

module Main where

import qualified Data.ByteString as BS
import Data.Char
import Data.Either.Combinators
import Data.Functor
import Data.Serialize
import GHC.Generics
import System.Exit
import System.IO

import Enemies
import Player

-- For testing serialization
defaultPlayer = Player
  { hp          = 10
  , maxHP       = 10
  , mp          = 5
  , maxMP       = 5
  , xp          = 0
  , shallots    = 0
  , playerClass = ChrisChan
  }


defaultGameState = GameState
  { player   = defaultPlayer
  , location = Town
  }


-- TODO: impl battles
type Battle = ()

data GameState = GameState
  { player   :: Player
  , location :: Location
  } deriving (Generic, Show)

instance Serialize GameState where

data Location = Town
              | Shop
              | Wilderness Battle
              | Dungeon Battle
              deriving (Generic, Show)

instance Serialize Location where

main :: IO ()
main = startupPrompt >>= play
  where
    play (Just game) = continue game
    play Nothing     = newGame


-- | Prompt the user and load a save game if the player so chooses.
startupPrompt :: IO (Maybe GameState)
startupPrompt = do
  putStrLn "(C)ontinue"
  putStrLn "(N)ew Game"
  putStrLn "(L)oad Game"
  putStrLn "(Q)uit"
  response <- prompt ""
  case toUpper $ head response of
    'C' -> loadGame "last_save.shallot"
    'N' -> return Nothing
    'L' -> prompt "Please enter the name of the save data" >>= loadGame
    'Q' -> exitSuccess
    _   -> putStrLn "???" >> startupPrompt


{- All of these calls to textbox have given me a brilliant idea for 
 - how I could overengineer this:
 - DIALOG BOX DSL!!!!!!!!
 -
 - Take a [String] and a parser that checks for "[name]" in the 
 - string that returns a [(Maybe Name, String)] to be folded over.
 - Why split it into a tuple? I'm considering giving the textboxes headers.
 -
 - dialogSeq = foldr (>>) (return ()) . parseDialog
 - 
 - We can split dialog sequences where branches occur.
 - Dialog is a small part of the game, and I need to remember that flexible
 - but literal code is better than a shitty restrictive abstraction.
 -}
newGame :: IO ()
newGame = do
  textbox "Ouch!"
  textbox "Your senses rush back to you as you take in your surroundings."
  textbox "What could you have possibly been doing to end up stranded in the woods?"
  textbox "And god damn! You haven't showered in weeks, have you?"
  textbox "You decide you ought to get back to civilization."
  textbox "..."
  textbox "......"
  textbox "........."
  textbox "Ah! A small town!"
  textbox "There's a perfectly generic adventurer's guild to boot!"
  textbox "As generic as generic gets. Ideal for your needs."
  locationFlash Town


continue = undefined

-- Utils

-- | Load a .shallot file and continue where you left off.
--
-- I really should change the type signature on this thing, right now it
-- will just silently fail and start a new game if you feed it a file that
-- fails to parse. The error message is also immediately discarded, bad practice!
loadGame :: FilePath -> IO (Maybe GameState)
loadGame saveFile = BS.readFile saveFile <&> (rightToMaybe . decode)


saveGame :: FilePath -> GameState -> IO ()
saveGame saveFile = BS.writeFile saveFile . encode 


textbox :: String -> IO ()
textbox msg = putStr msg >> hFlush stdout >> void getLine


-- Okay so imagine this:
-- Prompt automatically reads fields on ADT
-- Takes their names and turns them into options it knows how to parse.
-- Metaprogamming magic.
--
-- This language really makes me want to overengineer everything, huh.
-- Also that would involve creating a bunch of types, and how much flexibility
-- am I really buying? I'll still need to manually implement the response to each
-- ADT value.
prompt :: String -> IO String
prompt msg = putStrLn msg >> putStr "> " >> hFlush stdout >> getLine


locationFlash :: Location -> IO ()
locationFlash loc = do
  putStrLn $ replicate (locNameLen + 2) '-'
  putStrLn $ "|" ++ locationName ++ "|"
  putStrLn $ replicate (locNameLen + 2) '-'
    where
      locationName = 
        case loc of
          Town         -> "    SHALLOT TOWN    "
          Shop         -> "        TESCO       "
          Dungeon _    -> " BLEAK FALLS BARROW "
          Wilderness _ -> "  VALLEY OF DRAKES  "
      locNameLen = length locationName


-- | Runescape's XP curve, stolen from here: 
-- https://www.davideaversa.it/blog/gamedesign-math-rpg-level-based-progression/
xpCurve :: Int -> Int
xpCurve level = (sum [levelXP $ fromIntegral n | n <- [1..level]]) `div` 4
  where
    levelXP :: Float -> Int
    levelXP l = floor $ l + 300 * 2 ** (l / 7)
