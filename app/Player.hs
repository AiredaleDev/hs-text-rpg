{-# LANGUAGE DeriveGeneric #-}

module Player where

import Data.Serialize
import GHC.Generics

data Class = OnePunchMan
           | XXXPROMLGSWAGQUIKSCOPEXXX
           | ChrisChan
           deriving (Generic, Show)

instance Serialize Class where

data Player = Player
  { hp          :: Int
  , maxHP       :: Int
  , mp          :: Int
  , maxMP       :: Int
  , xp          :: Int
  , shallots    :: Int
  , playerClass :: Class
  } deriving (Generic, Show)

instance Serialize Player where

type Effect = Player -> Player

data Item = Item 
  { itemName   :: String
  , itemDesc   :: String
  , itemEffect :: Effect
  }

